function buildArrayNumbersUpTo(number) {
  const arrayNumbers = [];
  for (let i = 0; i < number; i++) {
    arrayNumbers.push(i);
  }
  return arrayNumbers;
}

function sumOfAllMultiplesOf3Or5Below(number) {
  return buildArrayNumbersUpTo(number)
    .filter(n => n % 3 === 0 || n % 5 === 0)
    .reduce((total, current) => total + current);
}

console.log(sumOfAllMultiplesOf3Or5Below(1000));
