const findFactors = number => {
    let divider = 2;
    const resp = [];
    while (number > 1) {
        if (number % divider === 0) {
            number = number / divider;
            resp.push(divider);
            divider = 1;
        }
        divider++;
    }
    return Math.max(...resp);
}

const resp = findFactors(600851475143);
console.log(resp)