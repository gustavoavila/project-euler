export function getDifBetweenSumOfSqtrAndSqtrOfSumOfFirst(number) {
  let count = 1,
    sumOfSqtr = 0,
    sqtrOfSum = 0;

  while (count <= number) {
    sumOfSqtr += Math.pow(count, 2);
    sqtrOfSum += count;
    count++;
  }

  return Math.pow(sqtrOfSum, 2) - sumOfSqtr;
}
