import { getDifBetweenSumOfSqtrAndSqtrOfSumOfFirst } from "../code06";

describe("Função Diferença entre o quadrado da soma e a soma dos quadrados", () => {
  it("se eu passar 10, deve retornar a diferença entre a soma dos quadrados e do quadrado da soma dos 10 primeiros números naturais", () => {
    const result = getDifBetweenSumOfSqtrAndSqtrOfSumOfFirst(10);
    expect(result).toBe(2640);
  });
});
