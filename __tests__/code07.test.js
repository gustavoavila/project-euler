import { getPrimeNumberOnPosition } from '../code07';

describe('Função que busca o enésimo número primo', () => {
  it('Se eu passar o número 6, deve retornar o valor do sexto número primo natural', () => {
    const result = getPrimeNumberOnPosition(6);
    expect(result).toBe(13);
  });
});
