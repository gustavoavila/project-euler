import { getLargestProductInASeries } from '../code08'

describe('Função que recebe a quantidade de dígitos e retorna o maior produto entre eles', () => {
  it('passando o valor 4, deve retornar o maior produto dentre todos os números fornecidos', () => {
    const result = getLargestProductInASeries(4)
    expect(result).toBe(5832)
  })
})
