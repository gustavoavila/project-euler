function isPalindromicNumber(number) {
  const numberStr = number.toString();
  let res;
  let f = 0;
  let l = numberStr.length - 1;

  while (l - f >= 1) {
    if (numberStr[f] === numberStr[l]) {
      res = true;
    } else {
      res = false;
      break;
    }
    f += 1;
    l -= 1;
  }

  return res;
}

function findLargestPalindromeFromProductOf3DigitNumbers() {
  let x = 999;
  let y = 999;
  let results = [];
  for (x; x > 99; x--) {
    y = 999;
    for (y; y > 99; y--) {
      let prod = x * y;
      if (isPalindromicNumber(prod)) {
        results.push(prod);
      }
    }
  }
  return Math.max(...results);
}

console.log(findLargestPalindromeFromProductOf3DigitNumbers());
