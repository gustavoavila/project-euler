function fib() {
  let a = 1;
  let b = 2;
  let results = [a, b];
  let aux = 0;
  while (b < 4000000) {
    aux = b;
    b = a + b;
    a = aux;
    results.push(b);
  }
  results.pop();
  return results
    .filter(n => n % 2 === 0)
    .reduce((total, current) => total + current);
}

console.log(fib());
