export function getPrimeNumberOnPosition(number) {
  let primeFactors = [];
  let candidate = 2;

  while (primeFactors.length < number) {

    let countDividers = 0;
    for (let i = candidate; i > 0; i--) {
      if (candidate % i === 0) {
        countDividers++;
      }
    }

    if (countDividers <= 2) {
      primeFactors.push(candidate);
    }

    candidate++;
  }
  return primeFactors[number - 1];
}
