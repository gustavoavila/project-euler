function smallestNumberDividedBy(initialNumber, finalNumber) {
  let notFound = true;
  let result = 0;
  let n = finalNumber;
  while (notFound) {
    for (let i = initialNumber; i <= finalNumber; i++) {
      if (n % i === 0) {
        if (i === finalNumber) {
          notFound = false;
          result = n;
        }
      } else {
        break;
      }
    }
    n = n + 1;
  }
  return result;
}

console.log(smallestNumberDividedBy(1, 20));
